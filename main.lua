-- bootstrap the compiler
fennel = require("lib.fennel")
debug.traceback = fennel.traceback
table.insert(package.loaders, fennel.make_searcher({correlate=true}))
fv = fennel.view
pp = function(x) print(fv(x)) end
lume = require("lib.lume")

_coroutine_resume = coroutine.resume
function coroutine.resume(...)
	local state,result = _coroutine_resume(...)
	if not state then
		error( tostring(result), 2 )	-- Output error message
	end
	return state,result
end

require("vendor.lite.main")
require("wrap")
