(local util (require :lib.util))

(local mm {})

(fn mm.__call [{: module : name} ...]
  (let [dispatcher (. mm.dispatchers module name)
        key (dispatcher ...)
        method (or (. mm.methods module name key) (. mm.methods module name :default))]
    (method ...)))

(fn mm.defmulti [dispatcher name module]
  (util.nested-tset mm [:dispatchers module name] dispatcher)
  (setmetatable {: module : name} mm))

(fn mm.defmethod [{: module : name} key method]
  (util.nested-tset mm [:methods module name key] method))

mm
