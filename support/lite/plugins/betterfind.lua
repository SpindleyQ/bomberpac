local core = require "core"
local command = require "core.command"
local keymap = require "core.keymap"
local CommandView = require "core.commandview"

local function is_finding()
  return core.active_view:is(CommandView) and core.active_view.label == "Find Text: "
end

command.add(is_finding, {
  ["better-find:done"] = function() core.active_view:submit() end,
  ["better-find:next"] = function()
    local commandview = core.active_view
    commandview.state.submit(commandview:get_text())
    core.set_active_view(core.last_active_view)
    command.perform("find-replace:repeat-find")
    core.set_active_view(commandview)
  end
})

keymap.add {
  ["escape"] = "better-find:done",
  ["return"] = "better-find:next"
}

