(local editor (require :editor))
(local game (require :game))
(local util (require :lib.util))
(local lume (require :lib.lume))
(local core (require :core))
(local command (require :core.command))
(local keymap (require :core.keymap))
(local translate (require :core.doc.translate))

(command.add "core.docview" {
  "fennel:eval" #(editor.inline-eval #(fv (fennel.eval $1 {:env _G :compiler-env _G}) {}))
  "lume:hotswap" (fn []
    (local modname
      (-> core.active_view.doc.filename
        (: :gsub "%.%a+$" "")
        (: :gsub "/" ".")
        (: :gsub "^data%." "")
        (: :gsub "%.init$" "")))
    (core.log (.. "Hotswapping " modname))
    (local (mod err) (util.hotswap modname))
    (when (not= err nil) (print err) (error err)))
})
(keymap.add {
  "alt+e" "fennel:eval"
  "alt+r" "lume:hotswap"
})

{}
