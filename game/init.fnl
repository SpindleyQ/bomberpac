(local util (require :lib.util))
(local modes (require :editor.lovemode))
(local ModeView (require :editor.modeview))
(local state (require :game.state))
(local core (require :core))
(local command (require :core.command))

(local gamemode (require :game.mode))
(local SpaceOrb (require :game.orb))
(local map (require :game.tilemap))
(local tile (require :game.tiles))
(local bomberman (require :game.entities.bomberman))
(local pacman (require :game.entities.pacman))
(local rules (require :game.rules))

(modes:register :game gamemode)

(set state.entities [(bomberman.new [48 48]) (pacman.new [112 112])])
(set state.map (map.new-tilemap 28 31 tile.bombertile (tile.itile-named tile.bombertile :empty)))
(set state.bombs (map.new-entitymap state.map.w state.map.h))
(rules.generate-maze state.map)

(command.add nil {
  "love:game" (fn []
    (let [node (core.root_view:get_active_node)]
      (node:add_view (ModeView gamemode))))
  "love:regenerate-maze" #(rules.generate-maze state.map)
})

{}
