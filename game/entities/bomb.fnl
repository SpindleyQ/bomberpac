(local util (require :lib.util))
(local dim (require :game.dim))
(local {: defmethod} (util.require :lib.multimethod))
(local {: update} (util.require :game.entity))
(local {: drawtile} (util.require :game.tilemap))

(local Bomb (util.hot-table ...))

(fn draw-ticking [bomb x y]
  (love.graphics.setColor 0.4 0.4 0.4)
  (love.graphics.circle :fill x y (/ dim.tilesize 2))
  (love.graphics.setColor 1 0.3 0.3 1)
  (love.graphics.print (tostring (math.ceil bomb.timer)) (- x 4) (- y 10)))

(fn draw-exploding [bomb x y]
  (let [radius (/ dim.tilesize 2)
        l (- x radius)
        t (- y radius)]
    (love.graphics.setColor 1 0.7 0)
    (love.graphics.rectangle :fill l t dim.tilesize dim.tilesize)))

(defmethod drawtile :bomb (fn [bomb x y]
  (match bomb.state
    :ticking (draw-ticking bomb x y)
    :exploding (draw-exploding bomb x y))))

(fn Bomb.set-state [bomb state time]
  (set bomb.timer time)
  (set bomb.state state)
  state)

(fn Bomb.explode [bomb rules]
  (Bomb.set-state bomb :exploding 0.5)
  (set bomb.bomb nil)
  (set bomb.deadly true)
  (rules.explode-bomb bomb))

(defmethod update :bomb (fn [bomb dt rules]
  (set bomb.timer (- bomb.timer dt))
  (when (<= bomb.timer 0)
    (match bomb.state
      :ticking (Bomb.explode bomb rules)
      :exploding (rules.clear-bomb bomb)))))

(fn Bomb.new []
  {:state :ticking :timer 3 :bomb true :entity :bomb})

(fn Bomb.new-explosion []
  {:state :exploding :timer 0.5 :deadly true :entity :bomb})

Bomb.hot
