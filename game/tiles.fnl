(local dim (require :game.dim))
(local util (require :lib.util))
(local {: defmethod} (util.require :lib.multimethod))
(local {: drawtile} (util.require :game.tilemap))

(local tileset (util.hot-table ...))
(fn rect [x y color]
  (love.graphics.setColor (table.unpack color))
  (love.graphics.rectangle :fill
                            (- x (/ dim.tilesize 2))
                            (- y (/ dim.tilesize 2))
                            dim.tilesize
                            dim.tilesize))
(fn tileset.make-tileset [tiles]
  (each [itile tile (ipairs tiles)]
    (tset tiles tile.name itile))
  tiles)

(fn tileset.itile-named [tileset name]
  (. tileset name))

(defmethod drawtile :strongwall (fn [tile x y] (rect x y [0.7 0.7 0.7])))
(defmethod drawtile :weakwall (fn [tile x y] (rect x y [0.4 0.4 0.4])))
(defmethod drawtile :dot (fn [tile x y]
  (love.graphics.setColor 1 1 1)
  (love.graphics.circle :fill x y (/ dim.tilesize 8))))

(set tileset.bombertile (tileset.make-tileset
  [{:name :empty
    :empty true}
   {:name :strongwall
    :wall true}
   {:name :weakwall
    :wall true
    :breakable true}
   {:name :dot
    :edible true}
  ]))

tileset.hot

