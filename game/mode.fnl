(local util (require :lib.util))
(local state (require :game.state))
(local map (require :game.tilemap))
(local rules (require :game.rules))
(local {: draw : update} (util.require :game.entity))

(fn game-update [dt]
  (map.update-entitymap state.bombs dt rules)
  (each [_ entity (ipairs state.entities)]
    (update entity dt rules)))

(fn exception-update [dt]
  (when (love.keyboard.isDown :f2)
    (set state.update-exception nil)))

(fn update [dt]
  (if state.update-exception
    (exception-update dt)
    (xpcall #(game-update dt) (fn [msg]
      (set state.update-exception (.. msg "\n" (debug.traceback)))))))

(fn game-draw []
  (map.draw-tilemaps 0 0 [state.bombs state.map])
  (each [_ entity (ipairs state.entities)]
    (draw entity)))

(fn exception-draw []
  (love.graphics.setColor 1 0 0 1)
  (love.graphics.setNewFont 14)
  (love.graphics.printf (.. "Press F2 when resolved\n" state.update-exception) 20 20 (- (love.graphics.getWidth) 40)))

(fn draw []
  (if state.update-exception
    (exception-draw)
    (game-draw)))

{: update : draw}

