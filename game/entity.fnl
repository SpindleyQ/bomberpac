(local util (require :lib.util))
(local {: defmulti : defmethod} (util.require :lib.multimethod))
(local {: vec* : vec+ : dir-from-key} (util.require :game.helpers))
(local dim (require :game.dim))

(local entity {})
(fn entity.move [pos vel dt]
  (vec+ pos (vec* vel dt)))

(fn entity.direct [keymap speed]
  (vec* (dir-from-key keymap) speed))

(set entity.draw (defmulti #(or $1.draw $1.entity) :draw ...))
(set entity.update (defmulti #(or $1.update $1.entity) :update ...))
(defmethod entity.draw :default #nil)
(defmethod entity.update :default #nil)

entity

