(local Object (require :core.object))

(local SpaceOrb (Object:extend))
(fn SpaceOrb.new [self joystick]
  (set self.joystick joystick))

; axes (with controller upright)
; x: -left +right
; z: -forward +backward
; y: -up +down
; xr: -up +down (twisting)
; yr: -right +left (twisting)
; zr: -right +left (turning)
(fn SpaceOrb.read [self]
  (local (x z y xr yr zr) (self.joystick:getAxes))
  {: x : y : z : xr : yr : zr})
(fn SpaceOrb.debug-draw [self]
  (var iaxis 1)
  (each [axis val (pairs (self:read))]
    (love.graphics.print (.. axis ": " (tostring val)) 10 (* iaxis 15))
    (set iaxis (+ iaxis 1))))
(fn SpaceOrb.update [self])
(fn SpaceOrb.draw [self] (self:debug-draw))

SpaceOrb

