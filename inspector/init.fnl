(local util (require :lib.util))
(local style (require :core.style))
(local {: defmulti : defmethod} (util.require :lib.multimethod))
(local {: textbutton : label : under : right-of : reform : group-wrapper  } (util.require :editor.imgui))

(local inspector (util.hot-table ...))

(fn inspector.best-inspector [v]
  (var best-inspector nil)
  (var best-priority -1)
  (each [inspector {: priority : predicate} (pairs inspector.inspectors)]
    (when (and (> priority best-priority) (predicate v))
      (set best-inspector inspector)
      (set best-priority priority)))
  best-inspector)

(set inspector.inspect
  (defmulti (fn [form state value]
              (when (= state.inspector nil)
                (set state.inspector (inspector.best-inspector value)))
              state.inspector) :inspect ...))

(fn inspector.register [name priority predicate inspect-func]
  (when (= inspector.inspectors nil)
    (set inspector.inspectors {}))
  (tset inspector.inspectors name {: predicate : priority :inspector inspect-func})
  (defmethod inspector.inspect name inspect-func))

(inspector.register :default 0 #true (fn [form state value]
  (label (reform form {:font style.code_font}) (fv value))))

(inspector.register :table 10
  #(and (= (type $1) :table) (not= (next $1) nil))
  (fn [form state tbl]
    (let [get-kstate (fn [tbl k state]
            (when (= nil state.keys) (set state.keys {}))
            (when (= nil (?. state.keys k))
              (util.nested-tset state [:keys k] {:collapsed (= (type (. tbl k)) :table) :children {}}))
            (. state.keys k))
          g (group-wrapper form)]
      (each [k v (pairs tbl)]
        (let [kstate (get-kstate tbl k state)]
          ; todo: state assumes an .inspector key
          ; todo: inspector swapping
          ; todo: edit in place?
          (when (g textbutton (under form {:font style.code_font}) (fv k))
            (set kstate.collapsed (not kstate.collapsed)))
          (if kstate.collapsed
             (g label (right-of form {:color style.syntax.comment :into {}}) "...")
             (g #(inspector.inspect $...) (right-of form {:into {}}) kstate.children v))
          (g))))))

inspector.hot

