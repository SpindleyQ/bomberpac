(local core (require :core))
(local style (require :core.style))
(local util (require :lib.util))
(local repl (require :editor.repl))
(local ReplView (require :editor.replview))

(local module (util.hot-table ...))

(fn find-existing-inspector-window [name]
  (var result nil)
  (each [_ view (ipairs (core.root_view.root_node:get_children)) :until result]
    (when (= view.inspector-name name)
      (set result view)))
  result)

(fn create-inspector-window [name ?value]
  (let [node (core.root_view:get_active_node)
        conn (repl.new)
        view (ReplView conn)]
    (set view.inspector-name name)
    (set view.title name)
    (view:append {:draw (fn [_ _ x y] (renderer.draw_text style.font name x y style.text) (+ (style.font:get_height) style.padding.y))})
    (view:append (repl.mk-result [?value]))
    (node:add_view view)))

(lambda module.show [name ?value]
  (when (= (find-existing-inspector-window name) nil)
    (create-inspector-window name ?value)))

module.hot
