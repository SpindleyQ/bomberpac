(local modes (require :editor.lovemode))
(local core (require :core))

(local events [])
(local editor-mode
  {:draw system.draw_love_frame
   :update (fn [...]
     (when (and core.active_view core.active_view.handle-love-update)
       (core.active_view:handle-love-update ...)))
   :handler (fn [...]
     (system.enqueue_love_event ...)
     (when (and core.active_view core.active_view.handle-love-event)
       (core.active_view:handle-love-event ...)))})

(modes:register :editor editor-mode)

{: editor-mode : events}
