(local util (require :lib.util))
(local fennel (require :lib.fennel))
(local style (require :core.style))
(local lume (require :lib.lume))
(local {: textbutton : under : group-wrapper} (util.require :editor.imgui))
(local {: inspect} (util.require :inspector))
(local repl (util.hot-table ...))

(fn repl.inspector [{: w &as form} {: vals : states}]
  (let [g (group-wrapper form)]
    (each [i v (ipairs vals)]
      (g #(inspect $...) (under (g) {: w}) (. states i) v))
    (g)))

(fn repl.notify [listeners line]
  (each [_ listener (ipairs listeners)]
    (listener:append line)))

(fn repl.mk-result [vals]
  {:draw repl.inspector : vals :states (icollect [_ (ipairs vals)] {})})

(fn repl.run [{: listeners}]
  (fennel.repl {:readChunk coroutine.yield
                :onValues #(repl.notify listeners (repl.mk-result $1))
                :onError (fn [errType err luaSource] (repl.notify listeners (repl.mk-result [err])))
                :pp #$1
                :env (lume.clone _G)}))

(fn repl.listen [{: listeners} listener]
  (table.insert listeners listener))

(fn repl.unlisten [{: listeners} listener]
  (lume.remove listeners listener))

(fn repl.submit [{: coro} chunk]
  (coroutine.resume coro (.. chunk "\n")))

(fn repl.new []
  (local result
    {:listeners []
     :listen #(repl.listen $...)
     :unlisten #(repl.unlisten $...)
     :submit #(repl.submit $...)
     :coro (coroutine.create repl.run)})
  (coroutine.resume result.coro result)
  result)

repl.hot
