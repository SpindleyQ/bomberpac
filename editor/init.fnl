(local util (require :lib.util))
(local core (require :core))
(local command (require :core.command))
(local keymap (require :core.keymap))
(local common (require :core.common))

(fn inline-eval [eval]
  (let [ldoc core.active_view.doc
        (aline acol bline bcol) (ldoc:get_selection)
        inject #(ldoc:insert bline bcol (eval $1))]
    (if (and (= aline bline) (= acol bcol))
      (inject (ldoc:get_text aline 1 aline 10000000))
      (inject (ldoc:get_text aline acol bline bcol)))))

(require :editor.editmode)

(command.add :editor.replview {
  "repl:submit" #(core.active_view:submit)
})

(local ReplView (require :editor.replview))
(local repl (require :editor.repl))
(command.add nil {
  "repl:create" (fn []
    (local node (core.root_view:get_active_node))
    (node:add_view (ReplView (repl.new)))
  )
})
(keymap.add {
  :return "repl:submit"
})

{: inline-eval}
