(local util (require :lib.util))
(local {: attach-imstate : textbox : textbutton : label : under : reform : group-wrapper : mouse-inside} (util.require :editor.imgui))
(local View (require :core.view))
(local style (require :core.style))

(local ReplView (View:extend))

(fn ReplView.new [self conn]
  (ReplView.super.new self)
  (attach-imstate self)
  (set self.conn conn)
  (set self.log [])
  (set self.cmd "")
  (set self.scrollheight math.huge)
  (set self.scrollable true)
  (set self.title "REPL")
  (self.conn:listen self))

(fn ReplView.try_close [self do_close]
  (self.conn:unlisten self)
  (ReplView.super.try_close self do_close))

(fn ReplView.get_scrollable_size [self] self.scrollheight)

(fn ReplView.append [self line]
  (table.insert self.log line))

(fn ReplView.draw-cmd [{: x : y : w : view &as form} {: cmd} iline]
  (label form cmd)
  (when (mouse-inside x y w form.h)
    (when (textbutton (reform form {:x (+ x w -35) :into {}}) :X)
      (table.remove view.log iline)
      (table.remove view.log iline))
    (when (textbutton (reform form {:x (+ x w -60) :into {}}) :!)
      (view:submit cmd))))

(fn ReplView.submit [self ?cmd]
  (local cmd (or ?cmd self.cmd))
  (when (= ?cmd nil)
    (set self.cmd ""))
  (self:append {:draw self.draw-cmd : cmd})
  (self.conn:submit cmd))

(fn ReplView.draw [self]
  (self:draw_background style.background)
  (self:draw_scrollbar)
  (let [{: w &as form} (self:form)
        g (group-wrapper form)]
    ; todo: cache sizes and avoid drawing if offscreen?
    ; note: then offscreen items can't be focussed without further effort
    ; todo: draw line numbers
    (each [i line (ipairs self.log)]
      (g line.draw (under (g) {: w}) line i))
    (set self.cmd (g textbox (under (g) {: w :tag :command}) self.cmd))
    (self:end-scroll (g))))

(fn ReplView.get_name [self] self.title)

ReplView

