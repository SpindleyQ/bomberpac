(local modes (require :editor.lovemode))
(local View (require :core.view))

(local ModeView (View:extend))

(fn ModeView.new [self mode]
  (ModeView.super.new self)
  (set self.mode mode))

(fn ModeView.draw [self]
  (love.graphics.push :all)
  (love.graphics.translate self.position.x self.position.y)
  (modes.draw self.mode)
  (love.graphics.pop))

(fn ModeView.handle-love-update [self ...]
  (when self.mode.update (self.mode.update ...)))

(fn ModeView.handle-love-event [self ev ...]
  (if self.mode.handler
    (self.mode.handler ev ...)
    ((. modes.std-handlers ev) ...)))

ModeView

